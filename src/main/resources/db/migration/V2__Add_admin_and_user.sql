insert into usr (username, password, email, first_name, user_image, status, created, updated)
    values ('mikita.admin', '$2a$04$VVV1wXNi.piok/S.tMMmTuKM0V3gRMfqeVB09afdMu91VmGDilJKm', 'm.lintsevich.vironit@vironit.ru', 'Mikita', null, 'ACTIVE', '2020-01-09 11:11:11.000000', '2020-01-09 11:11:11.000000');

insert into user_role (user_id, roles)
    values (1, 'USER'), (1, 'ADMIN');

insert into usr (username, password, email, first_name, user_image, status, created, updated)
    values ('maria.florist', '$2a$04$2qZAzrsHrqooAJfW8ha/kOcABzYJ0hD/btPS3mU8HJ9U4NCghJJWS', 'm.florist.vironit@vironit.ru', 'Maria', null, 'ACTIVE', '2020-01-09 11:11:11.000000', '2020-01-09 11:11:11.000000');

insert into user_role (user_id, roles)
    values (2, 'USER');