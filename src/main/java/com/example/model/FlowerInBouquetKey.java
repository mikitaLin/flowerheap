package com.example.model;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class FlowerInBouquetKey implements Serializable {

    private Long bouquetId;
    private Long flowerId;
}