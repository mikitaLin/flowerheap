package com.example.model;

public enum UserStatus {

    ACTIVE, NOT_ACTIVE
}