package com.example.model.entity;

import com.example.model.FlowerInBouquet;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bouquet")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString(exclude = {"flowers"})
public class Bouquet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "design_id")
    private Design design;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    private Order order;

    @Column(name = "bouquet_quantity")
    private Short bouquetQuantity;

    @OneToMany(mappedBy = "bouquet", fetch = FetchType.LAZY)
    private List<FlowerInBouquet> flowers;
}