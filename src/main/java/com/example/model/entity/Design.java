package com.example.model.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "design")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString(exclude = {"bouquets"})
public class Design {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "design_image")
    private String filename;

    @Column(name = "price")
    private BigDecimal price;

    @OneToMany(mappedBy = "design", fetch = FetchType.LAZY)
    private List<Bouquet> bouquets;
}