package com.example.model;

import com.example.model.entity.Bouquet;
import com.example.model.entity.Flower;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "flower_in_bouquet")
@Data
@EqualsAndHashCode(exclude = {"flower", "bouquet"})
@ToString(exclude = {"flower", "bouquet"})
public class FlowerInBouquet {

    @EmbeddedId
    private FlowerInBouquetKey id;

    @MapsId(value = "bouquetId")
    @ManyToOne
    private Bouquet bouquet;

    @MapsId(value = "flowerId")
    @ManyToOne
    private Flower flower;

    @Column(name = "flower_quantity")
    private Short flowerQuantity;
}