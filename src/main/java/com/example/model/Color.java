package com.example.model;

public enum Color {

    RED, ORANGE, YELLOW, GREEN, BLUE, PURPLE, WHITE, PINK, BLACK, MULTICOLOR
}