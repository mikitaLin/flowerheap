package com.example.model;

public enum OrderStatus {

    CONFIRMED, UNCONFIRMED, DELETED
}