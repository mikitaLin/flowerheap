package com.example.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.stream.Stream;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    private static final long MAX_AGE_SECS = 3600;
    private static final String PATH_PATTERN = "/**";
    private static final String ORIGINS = "*";

    private enum AllowedMethods {
        HEAD, OPTIONS, GET, POST, PUT, PATCH, DELETE;
        public static String[] getNames() {
            return Stream.of(AllowedMethods.values())
                    .map(AllowedMethods::name)
                    .toArray(String[]::new);
        }
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(PATH_PATTERN)
                .allowedOrigins(ORIGINS)
                .allowedMethods(AllowedMethods.getNames())
                .maxAge(MAX_AGE_SECS);
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)
                .setFieldMatchingEnabled(true)
                .setSkipNullEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
        return mapper;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}