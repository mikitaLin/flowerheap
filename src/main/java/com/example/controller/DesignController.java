package com.example.controller;

import com.example.dto.DesignDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.DesignAssembler;
import com.example.model.entity.Design;
import com.example.service.DesignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping(value = "/designs")
public class DesignController {

    private final DesignService designService;
    private final DesignAssembler assembler;

    @Autowired
    public DesignController(DesignService designService,
                            DesignAssembler assembler) {
        this.designService = designService;
        this.assembler = assembler;
    }

    @GetMapping
    public ResponseEntity<?> getAll(final PagedResourcesAssembler<Design> pagedResourcesAssembler,
                                    final Pageable pageable) {
        final Page<Design> designPage = designService.getAll(pageable);
        final PagedModel<DesignDto> designDtoList = pagedResourcesAssembler.toModel(designPage, assembler);
        log.info("IN getAll - All designs in amount {} was loaded", designPage.getTotalElements());
        return ResponseEntity.ok(designDtoList);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") final Long designId) {
        final DesignDto designDto = assembler.toModel(designService.getOne(designId));
        log.info("IN getOne - Design with id: {} was loaded", designId);
        return ResponseEntity.ok(designDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PostMapping
    public ResponseEntity<?> create(@Validated(ValidationInterface.Create.class) @RequestPart DesignDto designDto,
                                    @RequestPart("file") final MultipartFile file) {
        final Design design = assembler.toEntity(designDto, file);
        designDto = assembler.toModel(designService.create(design));
        log.info("IN create - Design with id: {} was loaded", designDto.getId());
        return ResponseEntity.ok(designDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") final Long designId,
                                    @Validated(ValidationInterface.Update.class) @RequestPart DesignDto designDto,
                                    @RequestPart(value = "file", required = false) final MultipartFile file) {
        final Design design = assembler.toEntity(designDto, file);
        designDto = assembler.toModel(designService.update(designId, design));
        log.info("IN update - Design with id: {} was updated", designId);
        return ResponseEntity.ok(designDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final Long designId) {
        designService.delete(designId);
        log.info("IN delete - Design with id: {} was deleted", designId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }
}