package com.example.controller;

import com.example.dto.TokenDto;
import com.example.dto.UserDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.UserAssembler;
import com.example.model.entity.User;
import com.example.security.jwt.JwtAuthenticationProvider;
import com.example.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtAuthenticationProvider tokenProvider;
    private final UserAssembler assembler;

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationManager authenticationManager,
                                    JwtAuthenticationProvider tokenProvider,
                                    UserAssembler assembler) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.assembler = assembler;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@Validated(ValidationInterface.Login.class) @RequestBody final UserDto userDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userDto.getUsername(),
                        userDto.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = tokenProvider.createToken(authentication);
        final TokenDto tokenDto = new TokenDto();
        tokenDto.setToken(token);
        tokenDto.setTokenType("Bearer");
        log.info("IN login - User with id: {} sign in", userService.getOne(authentication).getId());
        return ResponseEntity.ok(tokenDto);
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<?> registration(@Validated(ValidationInterface.Registration.class) @RequestBody UserDto userDto) {
        final User user = assembler.toEntity(userDto);
        userDto = assembler.toModel(userService.create(user));
        log.info("IN registration - User with id: {} was registered", userDto.getId());
        return ResponseEntity.ok(userDto);
    }
}