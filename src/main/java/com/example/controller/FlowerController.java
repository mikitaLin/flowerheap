package com.example.controller;

import com.example.dto.FlowerDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.FlowerAssembler;
import com.example.model.Color;
import com.example.model.entity.Flower;
import com.example.service.FlowerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping(value = "/flowers")
public class FlowerController {

    private final FlowerService flowerService;
    private final FlowerAssembler assembler;

    @Autowired
    public FlowerController(FlowerService flowerService,
                            FlowerAssembler assembler) {
        this.flowerService = flowerService;
        this.assembler = assembler;
    }

    @GetMapping
    public ResponseEntity<?> getAll(final PagedResourcesAssembler<Flower> pagedResourcesAssembler,
                                    final Pageable pageable,
                                    @RequestParam(required = false) final String type,
                                    @RequestParam(required = false) final Color color) {
        final Page<Flower> flowerPage = flowerService.getAll(pageable, type, color);
        final PagedModel<FlowerDto> flowerDtoList = pagedResourcesAssembler.toModel(flowerPage, assembler);
        log.info("IN get All - All flowers in amount {} was loaded", flowerPage.getTotalElements());
        return ResponseEntity.ok(flowerDtoList);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") final Long flowerId) {
        final FlowerDto flowerDto = assembler.toModel(flowerService.getOne(flowerId));
        log.info("IN getOne - Flower with id: {} was loaded", flowerId);
        return ResponseEntity.ok(flowerDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PostMapping
    public ResponseEntity<?> create(@Validated(ValidationInterface.Create.class) @RequestPart FlowerDto flowerDto,
                                    @RequestPart("file") final MultipartFile file) {
        final Flower flower = assembler.toEntity(flowerDto, file);
        flowerDto = assembler.toModel(flowerService.create(flower));
        log.info("IN create - Flower with id: {} was created", flowerDto.getId());
        return ResponseEntity.ok(flowerDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") final Long flowerId,
                                    @Validated(ValidationInterface.Update.class) @RequestPart FlowerDto flowerDto,
                                    @RequestPart(value = "file", required = false) final MultipartFile file) {
        final Flower flower = assembler.toEntity(flowerDto, file);
        flowerDto = assembler.toModel(flowerService.update(flowerId, flower));
        log.info("IN update - Flower with id: {} was updated", flowerId);
        return ResponseEntity.ok(flowerDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final Long flowerId) {
        flowerService.delete(flowerId);
        log.info("IN delete - Flower with id: {} was deleted", flowerId);
        return ResponseEntity.noContent().build();
    }
}