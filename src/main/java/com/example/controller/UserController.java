package com.example.controller;

import com.example.dto.UserDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.UserAssembler;
import com.example.model.Role;
import com.example.model.UserStatus;
import com.example.model.entity.User;
import com.example.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserAssembler assembler;
    private final UserService userService;

    @Autowired
    public UserController(UserAssembler assembler,
                          UserService userService) {
        this.assembler = assembler;
        this.userService = userService;
    }

    @GetMapping(value = "/activate/{code}")
    public ResponseEntity<?> activate(@PathVariable("code") final String code) {
        final UserDto userDto = assembler.toModel(userService.activateProfile(code));
        log.info("IN activate - User with id: {} was activated", userDto.getId());
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/send/{id}")
    public ResponseEntity<?> resendMessage(@PathVariable("id") final Long userId) {
        userService.resendMessage(userId);
        log.info("IN resendMessage - message was resend to user with id: {}", userId);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(final PagedResourcesAssembler<User> pagedResourcesAssembler,
                                    final Pageable pageable,
                                    @RequestParam(required = false) final UserStatus status,
                                    @RequestParam(required = false) final Role role,
                                    @RequestParam(required = false) final String firstName) {
        final Page<User> userPage = userService.getAll(pageable, status, role, firstName);
        final PagedModel<UserDto> userDtoList = pagedResourcesAssembler.toModel(userPage, assembler);
        log.info("IN getAll - All users in amount {} was loaded", userPage.getTotalElements());
        return ResponseEntity.ok(userDtoList);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") final Long userId) {
        final UserDto userDto = assembler.toModel(userService.getOne(userId));
        log.info("IN getOne - User with id: {} was loaded", userId);
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}/status")
    public ResponseEntity<?> changeStatus(@PathVariable("id") final Long userId,
                                          @Validated(ValidationInterface.changeStatus.class) @RequestBody UserDto userDto) {
        final User user = assembler.toEntity(userDto);
        userDto = assembler.toModel(userService.changeStatus(userId, user));
        log.info("IN changeStatus - User with id: {} was updated", userId);
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}/role")
    public ResponseEntity<?> changeRole(@PathVariable("id") final Long userId,
                                        @Validated(ValidationInterface.changeRole.class) @RequestBody UserDto userDto) {
        final User user = assembler.toEntity(userDto);
        userDto = assembler.toModel(userService.changeRole(userId, user));
        log.info("IN changeRole - User with id: {} was updated", userId);
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final Long userId) {
        userService.delete(userId);
        log.info("IN delete - User with id: {} was deleted", userId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/profile")
    public ResponseEntity<?> getProfile(final Authentication authentication) {
        final UserDto userDto = assembler.toModel(userService.getOne(authentication));
        log.info("IN getProfile - User with id: {} was loaded", userDto.getId());
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @PutMapping("/profile")
    public ResponseEntity<?> updateProfile(final Authentication authentication,
                                           @Validated(ValidationInterface.Update.class) @RequestBody UserDto userDto,
                                           @RequestParam(value = "file", required = false) final MultipartFile file) {
        final User user = assembler.toEntity(userDto, file);
        userDto = assembler.toModel(userService.updateProfile(authentication, user));
        log.info("IN updateProfile - User with id: {}, was updated", userDto.getId());
        return ResponseEntity.ok(userDto);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @DeleteMapping("/profile")
    public ResponseEntity<?> deleteProfile(final Authentication authentication) {
        userService.deleteProfile(authentication);
        log.info("IN deleteProfile - User with id: {} was deactivated", userService.getOne(authentication).getId());
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }
}