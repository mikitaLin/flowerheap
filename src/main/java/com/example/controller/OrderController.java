package com.example.controller;

import com.example.dto.OrderDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.OrderAssembler;
import com.example.model.OrderStatus;
import com.example.model.entity.Order;
import com.example.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/orders")
public class OrderController {

    private final OrderService orderService;
    private final OrderAssembler assembler;

    @Autowired
    public OrderController(OrderService orderService,
                           OrderAssembler assembler) {
        this.orderService = orderService;
        this.assembler = assembler;
    }

    @PostMapping
    public ResponseEntity<?> create(final Authentication authentication,
                                    @Validated(OrderDto.CreateOrder.class) @RequestBody OrderDto orderDto) {
        final Order order = assembler.toEntity(orderDto);
        orderDto = assembler.toModel(orderService.create(order, authentication));
        log.info("IN create - Order with id: {} was loaded", orderDto.getId());
        return ResponseEntity.ok(orderDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(final PagedResourcesAssembler<Order> pagedResourcesAssembler,
                                    final Pageable pageable,
                                    @RequestParam(required = false) final String phoneNumber,
                                    @RequestParam(required = false) final Long userId,
                                    @RequestParam(required = false) final OrderStatus status) {
        final Page<Order> orderPage = orderService.getAll(pageable, phoneNumber, userId, status);
        final PagedModel<OrderDto> orderDtoList = pagedResourcesAssembler.toModel(orderPage, assembler);
        log.info("IN getAll - All orders in amount {} was loaded", orderPage.getTotalElements());
        return ResponseEntity.ok(orderDtoList);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") final Long orderId) {
        final OrderDto orderDto = assembler.toModel(orderService.getOne(orderId));
        log.info("IN getOne - Order with id: {} was loaded", orderId);
        return ResponseEntity.ok(orderDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") final Long orderId,
                                    @Validated(ValidationInterface.Update.class) @RequestBody OrderDto orderDto) {
        final Order order = assembler.toEntity(orderDto);
        orderDto = assembler.toModel(orderService.update(orderId, order));
        log.info("IN update - Order with id: {} was updated", orderId);
        return ResponseEntity.ok(orderDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PutMapping(value = "/{id}/status")
    public ResponseEntity<?> changeStatus(@PathVariable("id") final Long orderId,
                                          @Validated(ValidationInterface.changeStatus.class) @RequestBody OrderDto orderDto) {
        final Order order = assembler.toEntity(orderDto);
        orderDto = assembler.toModel(orderService.changeStatus(orderId, order));
        log.info("IN changeStatus - Order with id: {} was updated", orderId);
        return ResponseEntity.ok(orderDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final Long orderId) {
        orderService.delete(orderId);
        log.info("IN delete - Order with id: {} was deleted", orderId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/profile")
    public ResponseEntity<?> getAllProfileOrders(final PagedResourcesAssembler<Order> pagedResourcesAssembler,
                                                 final Pageable pageable,
                                                 final Authentication authentication,
                                                 @RequestParam(required = false) final OrderStatus status) {
        final Page<Order> orderPage = orderService.getAll(pageable, authentication, status);
        final PagedModel<OrderDto> orderDtoList = pagedResourcesAssembler.toModel(orderPage, assembler);
        log.info("IN getAllProfileOrders - All orders in amount {} was loaded", orderPage.getTotalElements());
        return ResponseEntity.ok(orderDtoList);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/{id}/profile")
    public ResponseEntity<?> getOneProfileOrders(final Authentication authentication,
                                                 @PathVariable("id") final Long orderId) {
        final OrderDto orderDto = assembler.toModel(orderService.getOne(authentication, orderId));
        log.info("IN getOneProfileOrders - Order with id: {} was loaded", orderId);
        return ResponseEntity.ok(orderDto);
    }
}