package com.example.controller;

import com.example.dto.BouquetDto;
import com.example.dto.ValidationInterface;
import com.example.dto.assembler.BouquetAssembler;
import com.example.model.entity.Bouquet;
import com.example.service.BouquetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/bouquets")
public class BouquetController {

    private final BouquetService bouquetService;
    private final BouquetAssembler assembler;

    @Autowired
    public BouquetController(BouquetService bouquetService,
                             BouquetAssembler assembler) {
        this.bouquetService = bouquetService;
        this.assembler = assembler;
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping
    public ResponseEntity<?> getAll(final PagedResourcesAssembler<Bouquet> pagedResourcesAssembler,
                                    final Pageable pageable,
                                    @RequestParam(required = false) final Long orderId,
                                    @RequestParam(required = false) final Long userId) {
        final Page<Bouquet> bouquetPage = bouquetService.getAll(pageable, orderId, userId);
        final PagedModel<BouquetDto> bouquetDtoList = pagedResourcesAssembler.toModel(bouquetPage, assembler);
        log.info("IN getAll - All bouquets in amount {} was loaded", bouquetPage.getTotalElements());
        return ResponseEntity.ok(bouquetDtoList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") final Long bouquetId) {
        final BouquetDto bouquetDto = assembler.toModel(bouquetService.getOne(bouquetId));
        log.info("IN getOne - Bouquet with id: {} was loaded", bouquetId);
        return ResponseEntity.ok(bouquetDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @PostMapping(value = "/order/{id}")
    public ResponseEntity<?> create(@PathVariable("id") Long orderId,
                                    @Validated(ValidationInterface.CreateBouquet.class) @RequestBody BouquetDto bouquetDto) {
        final Bouquet bouquet = assembler.toEntity(bouquetDto);
        bouquetDto = assembler.toModel(bouquetService.create(bouquet, orderId));
        log.info("IN create - Bouquet with id: {} was added in order with id: {}", bouquetDto.getId(), orderId);
        return ResponseEntity.ok(bouquetDto);
    }

    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") final Long bouquetId) {
        bouquetService.delete(bouquetId);
        log.info("IN delete - Bouquet with id: {} was deleted", bouquetId);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/profile")
    public ResponseEntity<?> getAllProfileBouquets(final PagedResourcesAssembler<Bouquet> pagedResourcesAssembler,
                                                   final Pageable pageable,
                                                   final Authentication authentication,
                                                   @RequestParam(required = false) final Long orderId) {
        final Page<Bouquet> bouquetPage = bouquetService.getAll(pageable, authentication, orderId);
        final PagedModel<BouquetDto> bouquetDtoList = pagedResourcesAssembler.toModel(bouquetPage, assembler);
        log.info("IN getAllProfileBouquets - All bouquets in amount {} was loaded", bouquetPage.getTotalElements());
        return ResponseEntity.ok(bouquetDtoList);
    }

    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/{id}/profile")
    public ResponseEntity<?> getOneProfileBouquet(final Authentication authentication,
                                                  @PathVariable("id") final Long bouquetId) {
        final BouquetDto bouquetDto = assembler.toModel(bouquetService.getOne(authentication, bouquetId));
        log.info("IN getOneProfileBouquet - Bouquet with id: {} was loaded", bouquetId);
        return ResponseEntity.ok(bouquetDto);
    }
}