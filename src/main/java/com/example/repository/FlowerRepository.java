package com.example.repository;

import com.example.model.Color;
import com.example.model.entity.Flower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerRepository extends JpaRepository<Flower, Long> {

    Boolean existsByTypeAndColor(String type, Color color);
}