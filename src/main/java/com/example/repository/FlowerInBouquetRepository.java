package com.example.repository;

import com.example.model.FlowerInBouquet;
import com.example.model.FlowerInBouquetKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerInBouquetRepository extends JpaRepository<FlowerInBouquet, FlowerInBouquetKey> {
}