package com.example.repository;

import com.example.model.entity.Bouquet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BouquetRepository extends JpaRepository<Bouquet, Long> {

    @Query(
            value = "SELECT * FROM bouquet b LEFT JOIN ordr o ON b.order_id = o.id WHERE o.user_id = ?1",
            nativeQuery = true
    )
    List<Bouquet> findByUser(Long userId);

    @Query(
            value = "SELECT * FROM bouquet b LEFT JOIN ordr o ON b.order_id = o.id WHERE o.user_id = ?1 AND b.id = ?2",
            nativeQuery = true
    )
    Optional<Bouquet> findByUserAndId(Long userId, Long bouquetId);
}