package com.example.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class BouquetDto extends RepresentationModel<BouquetDto> implements ValidationInterface {

    @Null
    private Long id;

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private DesignDto design;

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private List<FlowerInBouquetDto> flowers;

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private Short bouquetQuantity;
}