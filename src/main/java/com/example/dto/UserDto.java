package com.example.dto;

import com.example.model.Role;
import com.example.model.UserStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDto extends RepresentationModel<UserDto> implements ValidationInterface {

    @Null
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Null
    private Date created;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Null
    private Date updated;

    @Null
    @NotNull(groups = {changeStatus.class})
    private UserStatus status;

    @Null
    @NotNull(groups = {Registration.class, Login.class, Update.class})
    private String username;

    @Null
    @NotNull(groups = {Registration.class, Login.class, Update.class})
    private String password;

    @Null
    @NotNull(groups = {Registration.class, Update.class})
    @Email
    private String email;

    @Null
    private String activationCode;

    @Null
    @NotNull(groups = {Registration.class, Update.class})
    private String firstName;

    @Null
    private String filename;

    @Null
    @NotNull(groups = {changeRole.class})
    private Set<Role> roles;

    @Null
    private List<OrderDto> orders;
}