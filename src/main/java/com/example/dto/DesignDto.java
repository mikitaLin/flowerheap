package com.example.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class DesignDto extends RepresentationModel<DesignDto> implements ValidationInterface {

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private Long id;

    @Null
    @NotNull(groups = {Create.class, Update.class})
    private String type;

    @Null
    private String filename;

    @Null
    @NotNull(groups = {Create.class, Update.class})
    private BigDecimal price;
}