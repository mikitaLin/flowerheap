package com.example.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Null;

@Data
public class TokenDto {

    @Null
    private String token;

    @Null
    private String tokenType;
}