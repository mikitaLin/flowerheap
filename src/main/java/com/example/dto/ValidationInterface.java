package com.example.dto;

public interface ValidationInterface {

    interface Login {}
    interface Registration {}
    interface changeStatus {}
    interface changeRole {}

    interface Create {}
    interface Update {}

    interface CreateOrder {}
    interface CreateBouquet {}
}