package com.example.dto;

import com.example.model.Color;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class FlowerDto extends RepresentationModel<FlowerDto> implements ValidationInterface {

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private Long id;

    @Null
    @NotNull(groups = {Create.class, Update.class})
    private String type;

    @Null
    @NotNull(groups = {Create.class, Update.class})
    private Color color;

    @Null
    private String filename;

    @Null
    @NotNull(groups = {Create.class, Update.class})
    private BigDecimal price;
}