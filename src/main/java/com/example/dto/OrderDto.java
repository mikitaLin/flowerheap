package com.example.dto;

import com.example.model.OrderStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class OrderDto extends RepresentationModel<OrderDto> implements ValidationInterface {

    @Null
    @NotNull(groups = {CreateBouquet.class})
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Null
    private Date created;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Null
    private Date updated;

    @Null
    @NotNull(groups = {changeStatus.class})
    private OrderStatus status;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private Date requiredDate;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private String firstName;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private String lastName;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private String address;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private String phoneNumber;

    @Null
    @NotNull(groups = {CreateOrder.class, Update.class})
    private List<BouquetDto> bouquets;
}