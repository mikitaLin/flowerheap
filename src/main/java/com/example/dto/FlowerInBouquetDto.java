package com.example.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
class FlowerInBouquetDto implements ValidationInterface {

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private FlowerDto flower;

    @Null
    @NotNull(groups = {CreateBouquet.class, CreateOrder.class})
    private Short flowerQuantity;
}