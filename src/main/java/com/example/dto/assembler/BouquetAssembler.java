package com.example.dto.assembler;

import com.example.controller.BouquetController;
import com.example.dto.BouquetDto;
import com.example.model.entity.Bouquet;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BouquetAssembler extends RepresentationModelAssemblerSupport<Bouquet, BouquetDto> {

    private final ModelMapper mapper;

    public BouquetAssembler(ModelMapper mapper) {
        super(BouquetController.class, BouquetDto.class);
        this.mapper = mapper;
    }

    @Override
    public BouquetDto toModel(Bouquet bouquet) {
        BouquetDto bouquetDto = mapper.map(bouquet, BouquetDto.class);
        bouquetDto.add(
                linkTo(methodOn(BouquetController.class).getOne(bouquetDto.getId())).withSelfRel()
        );
        return bouquetDto;
    }

    public Bouquet toEntity(BouquetDto bouquetDto) {
        return mapper.map(bouquetDto, Bouquet.class);
    }
}