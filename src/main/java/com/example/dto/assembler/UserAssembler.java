package com.example.dto.assembler;

import com.example.controller.FileController;
import com.example.controller.UserController;
import com.example.dto.UserDto;
import com.example.model.entity.User;
import com.example.service.implementation.FileStorageServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserAssembler extends RepresentationModelAssemblerSupport<User, UserDto> {

    private final ModelMapper mapper;
    private final FileStorageServiceImpl fileStorageService;

    public UserAssembler(ModelMapper mapper,
                         FileStorageServiceImpl fileStorageService) {
        super(UserController.class, UserDto.class);
        this.mapper = mapper;
        this.fileStorageService = fileStorageService;
    }

    @Override
    public UserDto toModel(User user) {
        UserDto userDto = mapper.map(user, UserDto.class);
        final String filename = userDto.getFilename();
        if (filename != null) {
            userDto.add(
                    linkTo(methodOn(FileController.class).downloadFile(filename, null)).withRel("avatar")
                    );
        }
        userDto.add(
                linkTo(methodOn(UserController.class).getOne(userDto.getId())).withSelfRel()
        );
        return userDto;
    }

    public User toEntity(UserDto userDto) {
        return mapper.map(userDto, User.class);
    }

    public User toEntity(UserDto userDto, MultipartFile file) {
        if (file != null) {
            userDto.setFilename(fileStorageService.storeFile(file));
        }
        return mapper.map(userDto, User.class);
    }
}