package com.example.dto.assembler;

import com.example.controller.FileController;
import com.example.controller.FlowerController;
import com.example.dto.FlowerDto;
import com.example.model.entity.Flower;
import com.example.service.implementation.FileStorageServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class FlowerAssembler extends RepresentationModelAssemblerSupport<Flower, FlowerDto> {

    private final ModelMapper mapper;
    private final FileStorageServiceImpl fileStorageService;

    public FlowerAssembler(ModelMapper mapper,
                           FileStorageServiceImpl fileStorageService) {
        super(FlowerController.class, FlowerDto.class);
        this.mapper = mapper;
        this.fileStorageService = fileStorageService;
    }

    @Override
    public FlowerDto toModel(Flower flower) {
        FlowerDto flowerDto = mapper.map(flower, FlowerDto.class);
        flowerDto.add(
                linkTo(methodOn(FileController.class).downloadFile(flowerDto.getFilename(), null)).withRel("image"),
                linkTo(methodOn(FlowerController.class).getOne(flowerDto.getId())).withSelfRel()
        );
        return flowerDto;
    }

    public Flower toEntity(FlowerDto flowerDto, MultipartFile file) {
        if (file != null) {
            flowerDto.setFilename(fileStorageService.storeFile(file));
        }
        return mapper.map(flowerDto, Flower.class);
    }
}