package com.example.dto.assembler;

import com.example.controller.OrderController;
import com.example.dto.OrderDto;
import com.example.model.entity.Order;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class OrderAssembler extends RepresentationModelAssemblerSupport<Order, OrderDto> {

    private final ModelMapper mapper;

    public OrderAssembler(ModelMapper mapper) {
        super(OrderController.class, OrderDto.class);
        this.mapper = mapper;
    }

    @Override
    public OrderDto toModel(Order order) {
        OrderDto orderDto = mapper.map(order, OrderDto.class);
        orderDto.add(
                linkTo(methodOn(OrderController.class).getOne(orderDto.getId())).withSelfRel()
        );
        return orderDto;
    }

    public Order toEntity(OrderDto orderDto) {
        return mapper.map(orderDto, Order.class);
    }
}