package com.example.dto.assembler;

import com.example.controller.DesignController;
import com.example.controller.FileController;
import com.example.dto.DesignDto;
import com.example.model.entity.Design;
import com.example.service.implementation.FileStorageServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DesignAssembler extends RepresentationModelAssemblerSupport<Design, DesignDto> {

    private final ModelMapper mapper;
    private final FileStorageServiceImpl fileStorageService;

    @Autowired
    public DesignAssembler(ModelMapper mapper,
                           FileStorageServiceImpl fileStorageService) {
        super(DesignController.class, DesignDto.class);
        this.mapper = mapper;
        this.fileStorageService = fileStorageService;
    }

    @Override
    public DesignDto toModel(Design design) {
        DesignDto designDto = mapper.map(design, DesignDto.class);
        designDto.add(
                linkTo(methodOn(FileController.class).downloadFile(designDto.getFilename(), null)).withRel("image"),
                linkTo(methodOn(DesignController.class).getOne(designDto.getId())).withSelfRel()
        );
        return designDto;
    }

    public Design toEntity(DesignDto designDto, MultipartFile file) {
        if (file != null) {
            designDto.setFilename(fileStorageService.storeFile(file));
        }
        return mapper.map(designDto, Design.class);
    }
}