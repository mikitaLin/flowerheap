package com.example.service;

import com.example.model.Role;
import com.example.model.UserStatus;
import com.example.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public interface UserService {

    User create(User user);

    Page<User> getAll(Pageable pageable, UserStatus status, Role role, String firstName);

    User getOne(Long userId);

    User getOne(String username);

    User getOne(Authentication authentication);

    Optional<User> findOne(Long userId);

    Optional<User> findOne(Authentication authentication);

    User changeStatus(Long userId, User user);

    User changeRole(Long userId, User user);

    void delete(Long userId);

    User activateProfile(String code);

    User updateProfile(Authentication authentication, User user);

    void deleteProfile(Authentication authentication);

    void resendMessage(Long userId);
}