package com.example.service;

import com.example.model.Color;
import com.example.model.FlowerInBouquet;
import com.example.model.entity.Flower;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FlowerService extends CrudService<Flower, Long> {

    Page<Flower> getAll(Pageable pageable, String type, Color color);

    FlowerInBouquet saveFlowerInBouquet(FlowerInBouquet flowerInBouquet);
}