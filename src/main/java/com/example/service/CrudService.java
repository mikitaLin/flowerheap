package com.example.service;

public interface CrudService<Entity, Key> {

    Entity create(Entity entity);

    Entity getOne(Key key);

    Entity update(Key key, Entity entity);

    void delete(Key key);
}