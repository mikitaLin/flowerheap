package com.example.service.implementation;

import com.example.exception.ResourceNotFoundException;
import com.example.model.OrderStatus;
import com.example.model.entity.Bouquet;
import com.example.model.entity.Order;
import com.example.repository.OrderRepository;
import com.example.service.BouquetService;
import com.example.service.OrderService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class OrderServiceImpl implements OrderService {

    private static final String ORDER_NOT_FOUND = "Order not found";

    private final OrderRepository orderRepository;

    private final BouquetService bouquetService;
    private final UserService userService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository,
                            BouquetService bouquetService,
                            UserService userService) {
        this.orderRepository = orderRepository;
        this.bouquetService = bouquetService;
        this.userService = userService;
    }

    @Override
    public Order create(Order order) {
        order = orderRepository.save(order);
        return order;
    }

    @Override
    public Order create(final Order order, final Authentication authentication) {
        order.setUser(userService.findOne(authentication).orElse(null));
        order.setCreated(new Date());
        order.setUpdated(new Date());
        order.setStatus(OrderStatus.UNCONFIRMED);
        orderRepository.save(order);
        final List<Bouquet> bouquets = order.getBouquets().stream()
                .map(bouquet -> {
                    bouquet.setOrder(order);
                    return bouquetService.create(bouquet);
                })
                .collect(toList());
        order.setBouquets(bouquets);
        return order;
    }

    @Override
    public Page<Order> getAll(final Pageable pageable,
                              final String phoneNumber,
                              final Long userId,
                              final OrderStatus status
    ) {
        final Order sortOrder = Order
                .builder()
                .phoneNumber(phoneNumber)
                .user(userService.findOne(userId).orElse(null))
                .status(status)
                .build();
        return orderRepository.findAll(Example.of(sortOrder), pageable);
    }

    @Override
    public Page<Order> getAll(final Pageable pageable,
                              final Authentication authentication,
                              final OrderStatus status
    ) {
        final Order sortOrder = Order
                .builder()
                .user(userService.findOne(authentication).orElse(null))
                .status(status)
                .build();
        return orderRepository.findAll(Example.of(sortOrder), pageable);
    }

    @Override
    public Order getOne(final Long orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException(ORDER_NOT_FOUND));
    }

    @Override
    public Order getOne(final Authentication authentication, final Long orderId) {
        return orderRepository.findByUserAndId(userService.getOne(authentication), orderId)
                .orElseThrow(() -> new ResourceNotFoundException(ORDER_NOT_FOUND));
    }

    @Override
    public Order update(final Long orderId, final Order updatedOrder) {
        return orderRepository.findById(orderId)
                .map(order -> {
                    order.setAddress(updatedOrder.getAddress());
                    order.setFirstName(updatedOrder.getFirstName());
                    order.setLastName(updatedOrder.getLastName());
                    order.setPhoneNumber(updatedOrder.getPhoneNumber());
                    order.setRequiredDate(updatedOrder.getRequiredDate());
                    return order;
                })
                .orElseThrow(() -> new ResourceNotFoundException(ORDER_NOT_FOUND));
    }

    @Override
    public void delete(final Long orderId) {
        if (!orderRepository.existsById(orderId)) {
            throw new ResourceNotFoundException(ORDER_NOT_FOUND);
        }
        orderRepository.deleteById(orderId);
    }

    @Override
    public Order changeStatus(final Long orderId, final Order updatedOrder) {
        return orderRepository.findById(orderId)
                .map(order -> {
                    order.setStatus(updatedOrder.getStatus());
                    order.setUpdated(new Date());
                    return order;
                })
                .orElseThrow(() -> new ResourceNotFoundException(ORDER_NOT_FOUND));
    }
}