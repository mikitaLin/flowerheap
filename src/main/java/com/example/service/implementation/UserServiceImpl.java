package com.example.service.implementation;

import com.example.exception.BadRequestException;
import com.example.exception.ResourceNotFoundException;
import com.example.model.Role;
import com.example.model.UserStatus;
import com.example.model.entity.User;
import com.example.repository.UserRepository;
import com.example.security.UserPrincipal;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.Collections.singleton;

@Service
public class UserServiceImpl implements UserService {

    private static final String USERNAME_TAKEN = "Username is already taken";
    private static final String EMAIL_IN_USE = "Email is already in use";
    private static final String USER_NOT_FOUND = "User not found";
    private static final String ACTIVATE_MESSAGE = "Hello, %s! Thanks for join. \n" +
            "Welcome to flowerHeap. To continue: http://localhost:8080/users/activate/%s";

    private final UserRepository userRepository;
    private final MailSenderImpl mailSender;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           MailSenderImpl mailSender,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new BadRequestException(USERNAME_TAKEN);
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            throw new BadRequestException(EMAIL_IN_USE);
        }

        user.setStatus(UserStatus.NOT_ACTIVE);
        user.setRoles(singleton(Role.USER));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setCreated(new Date());
        user.setUpdated(new Date());
        user = userRepository.save(user);

        sendMessage(user);
        return user;
    }

    @Override
    public Page<User> getAll(final Pageable pageable,
                             final UserStatus status,
                             final Role role,
                             final String firstName
    ) {
        Set<Role> roles = new HashSet<>();
        final User sortUser = User
                .builder()
                .status(status)
                .roles(roles)
                .firstName(firstName)
                .build();
        return userRepository.findAll(Example.of(sortUser), pageable);
    }

    @Override
    public User getOne(final Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public User getOne(final String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public User getOne(final Authentication authentication) {
        return findOne(authentication)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public Optional<User> findOne(final Long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> findOne(final Authentication authentication) {
        final UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        final Long userId = userPrincipal.getId();
        return userRepository.findById(userId);
    }

    @Override
    public User changeStatus(final Long userId, final User updatedUser) {
        return userRepository.findById(userId)
                .map(user -> {
                    user.setStatus(updatedUser.getStatus());
                    user.setUpdated(new Date());
                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public User changeRole(final Long userId, final User updatedUser) {
        final Set<Role> roles = updatedUser.getRoles();
        return userRepository.findById(userId)
                .map(user -> {
                    if (roles.contains(Role.ADMIN)) {
                        roles.clear();
                        roles.add(Role.USER);
                        roles.add(Role.ADMIN);
                        user.setRoles(roles);
                        user.setUpdated(new Date());
                    } else {
                        user.setRoles(roles);
                        user.setUpdated(new Date());
                    }
                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public void delete(final Long userId) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException(USER_NOT_FOUND);
        }
        userRepository.deleteById(userId);
    }

    @Override
    public User activateProfile(final String code) {
        return userRepository.findByActivationCode(code)
                .map(user -> {
                    user.setStatus(UserStatus.ACTIVE);
                    user.setActivationCode(null);
                    user.setUpdated(new Date());
                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public User updateProfile(final Authentication authentication, final User updatedUser) {
        final Long userId = getOne(authentication).getId();
        final String filename = updatedUser.getFilename();
        return userRepository.findById(userId)
                .map(user -> {
                    user.setUsername(updatedUser.getUsername());
                    user.setEmail(updatedUser.getEmail());
                    user.setPassword(passwordEncoder.encode(updatedUser.getPassword()));
                    user.setFirstName(updatedUser.getFirstName());
                    user.setFilename(filename == null ? user.getFilename() : filename);
                    user.setUpdated(new Date());
                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public void deleteProfile(final Authentication authentication) {
        findOne(authentication)
                .map(user -> {
                    user.setStatus(UserStatus.NOT_ACTIVE);
                    user.setUpdated(new Date());
                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public void resendMessage(final Long userId) {
        final User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND));
        user.setActivationCode(UUID.randomUUID().toString());
        sendMessage(user);
    }

    private void sendMessage(final User user) {
        final String message = String.format(ACTIVATE_MESSAGE,
                user.getUsername(),
                user.getActivationCode()
        );
        mailSender.sendMessage(user.getEmail(), "Activation code", message);
    }
}