package com.example.service.implementation;

import com.example.configuration.FileStorageProperties;
import com.example.exception.FileStorageException;
import com.example.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageServiceImpl {

    private static final String CREATE_CATALOG_EX = "Could not create the directory where the uploaded files will be stored";
    private static final String STORE_EX = "Could not store file. Please, try again";
    private static final String INVALID_PATH = "Sorry! Filename contains invalid path sequence ";
    private static final String FILE_NOT_FOUND = "File not found";

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageServiceImpl(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException(CREATE_CATALOG_EX, ex);
        }
    }

    public String storeFile(final MultipartFile file) {
        final String filename = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (filename.contains("..")) {
                throw new ResourceNotFoundException(INVALID_PATH + filename);
            }

            final Path targetLocation = this.fileStorageLocation.resolve(filename);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return filename;
        } catch (IOException ex) {
            throw new FileStorageException(STORE_EX);
        }
    }

    public Resource loadFileAsResource(final String filename) {
        try {
            final Path filePath = this.fileStorageLocation.resolve(filename).normalize();
            final Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new ResourceNotFoundException(FILE_NOT_FOUND);
            }
        } catch (MalformedURLException ex) {
            throw new ResourceNotFoundException(FILE_NOT_FOUND, ex);
        }
    }
}