package com.example.service.implementation;

import com.example.exception.ResourceNotFoundException;
import com.example.model.FlowerInBouquet;
import com.example.model.entity.Bouquet;
import com.example.repository.BouquetRepository;
import com.example.repository.DesignRepository;
import com.example.repository.OrderRepository;
import com.example.service.BouquetService;
import com.example.service.DesignService;
import com.example.service.FlowerService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class BouquetServiceImpl implements BouquetService {

    private static final String BOUQUET_NOT_FOUND = "Bouquet not found";
    private static final String DESIGN_NOT_FOUND = "Design not found";
    private static final String ORDER_NOT_FOUND = "Order not found";
    private static final String USER_NOT_FOUND = "User not found";

    private final BouquetRepository bouquetRepository;
    private final OrderRepository orderRepository;
    private final DesignRepository designRepository;

    private final FlowerService flowerService;
    private final UserService userService;

    @Autowired
    public BouquetServiceImpl(BouquetRepository bouquetRepository,
                              OrderRepository orderRepository,
                              DesignRepository designRepository,
                              FlowerService flowerService,
                              UserService userService) {
        this.bouquetRepository = bouquetRepository;
        this.orderRepository = orderRepository;
        this.designRepository = designRepository;
        this.flowerService = flowerService;
        this.userService = userService;
    }

    @Override
    public Bouquet create(final Bouquet bouquet) {
        final Long designId = bouquet.getDesign().getId();
        bouquet.setDesign(designRepository.findById(designId)
                .orElseThrow(() -> new ResourceNotFoundException(DESIGN_NOT_FOUND)));
        final List<FlowerInBouquet> flowers = bouquet.getFlowers().stream()
                .map(flowerInBouquet -> {
                    flowerInBouquet.setBouquet(bouquet);
                    return flowerService.saveFlowerInBouquet(flowerInBouquet);
                })
                .collect(toList());
        bouquet.setFlowers(flowers);
        return bouquet;
    }

    @Override
    public Bouquet create(final Bouquet bouquet, final Long orderId) {
        bouquet.setOrder(orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException(ORDER_NOT_FOUND)));
        return bouquetRepository.save(bouquet);
    }

    @Override
    public Page<Bouquet> getAll(final Pageable pageable, final Long orderId, final Long userId) {
        if (userId != null) {
            final List<Bouquet> bouquets = bouquetRepository.findByUser(userId);
            return new PageImpl<>(bouquets, pageable, bouquets.size());
        }
        if (orderId != null) {
            final Bouquet sortBouquet = Bouquet
                    .builder()
                    .order(orderRepository.findById(orderId).orElse(null))
                    .build();
            return bouquetRepository.findAll(Example.of(sortBouquet), pageable);
        }
        final List<Bouquet> bouquets = bouquetRepository.findAll();
        return new PageImpl<>(bouquets, pageable, bouquets.size());
    }

    @Override
    public Page<Bouquet> getAll(final Pageable pageable, final Authentication authentication, final Long orderId) {
        if (orderId != null) {
            final Bouquet sortBouquet = Bouquet
                    .builder()
                    .order(orderRepository.findById(orderId).orElse(null))
                    .build();
            return bouquetRepository.findAll(Example.of(sortBouquet), pageable);
        }
        final Long userId = userService.findOne(authentication).orElse(null).getId();
        final List<Bouquet> bouquets = bouquetRepository.findByUser(userId);
        return new PageImpl<>(bouquets, pageable, bouquets.size());
    }

    @Override
    public Bouquet getOne(final Long bouquetId) {
        return bouquetRepository.findById(bouquetId)
                .orElseThrow(() -> new ResourceNotFoundException(BOUQUET_NOT_FOUND));
    }

    @Override
    public Bouquet getOne(final Authentication authentication, final Long bouquetId) {
        final Long userId = userService.findOne(authentication)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND))
                .getId();
        return bouquetRepository.findByUserAndId(userId, bouquetId)
                .orElseThrow(() -> new ResourceNotFoundException(BOUQUET_NOT_FOUND));
    }

    @Override
    public Bouquet update(final Long bouquetId, final Bouquet updatedBouquet) {
        return bouquetRepository.findById(bouquetId)
                .map(bouquet -> {
                    bouquet.setDesign(updatedBouquet.getDesign());
                    bouquet.setOrder(updatedBouquet.getOrder());
                    bouquet.setFlowers(updatedBouquet.getFlowers());
                    bouquet.setBouquetQuantity(updatedBouquet.getBouquetQuantity());
                    return bouquet;
                })
                .orElseThrow(() -> new ResourceNotFoundException(BOUQUET_NOT_FOUND));
    }

    @Override
    public void delete(final Long bouquetId) {
        if (!bouquetRepository.existsById(bouquetId)) {
            throw new ResourceNotFoundException(BOUQUET_NOT_FOUND);
        }
        bouquetRepository.deleteById(bouquetId);
    }
}
