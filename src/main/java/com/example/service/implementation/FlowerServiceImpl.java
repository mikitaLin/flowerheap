package com.example.service.implementation;

import com.example.exception.BadRequestException;
import com.example.exception.ResourceNotFoundException;
import com.example.model.Color;
import com.example.model.FlowerInBouquet;
import com.example.model.FlowerInBouquetKey;
import com.example.model.entity.Flower;
import com.example.repository.FlowerInBouquetRepository;
import com.example.repository.FlowerRepository;
import com.example.service.FlowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
public class FlowerServiceImpl implements FlowerService {

    private static final String FLOWER_EXIST = "This flower is already created";
    private static final String FLOWER_NOT_FOUND = "Flower not found";

    private final FlowerRepository flowerRepository;
    private final FlowerInBouquetRepository flowerInBouquetRepository;

    @Autowired
    public FlowerServiceImpl(FlowerRepository flowerRepository,
                             FlowerInBouquetRepository flowerInBouquetRepository) {
        this.flowerRepository = flowerRepository;
        this.flowerInBouquetRepository = flowerInBouquetRepository;
    }

    @Override
    public Flower create(Flower flower) {
        if (flowerRepository.existsByTypeAndColor(flower.getType(), flower.getColor())) {
            throw new BadRequestException(FLOWER_EXIST);
        }
        flower = flowerRepository.save(flower);
        return flower;
    }


    @Override
    public Page<Flower> getAll(final Pageable pageable, final String type, final Color color) {
        final ExampleMatcher matcher = ExampleMatcher
                .matchingAll()
                .withMatcher("type", contains().ignoreCase());
        final Flower sortFlower = Flower
                .builder()
                .type(type)
                .color(color)
                .build();
        return flowerRepository.findAll(Example.of(sortFlower, matcher), pageable);
    }

    @Override
    public Flower getOne(final Long flowerId) {
        return flowerRepository.findById(flowerId)
                .orElseThrow(() -> new ResourceNotFoundException(FLOWER_NOT_FOUND));
    }

    @Override
    public Flower update(final Long flowerId, final Flower updatedFlower) {
        final String filename = updatedFlower.getFilename();
        return flowerRepository.findById(flowerId)
                .map(flower -> {
                    flower.setType(updatedFlower.getType());
                    flower.setColor(updatedFlower.getColor());
                    flower.setPrice(updatedFlower.getPrice());
                    flower.setFilename(filename == null ? flower.getFilename() : filename);
                    return flowerRepository.save(flower);
                })
                .orElseThrow(() -> new ResourceNotFoundException(FLOWER_NOT_FOUND));
    }

    @Override
    public void delete(final Long flowerId) {
        if (!flowerRepository.existsById(flowerId)) {
            throw new ResourceNotFoundException(FLOWER_NOT_FOUND);
        }
        flowerRepository.deleteById(flowerId);
    }

    @Override
    public FlowerInBouquet saveFlowerInBouquet(final FlowerInBouquet flowerInBouquet) {
        final Long flowerId = flowerInBouquet.getFlower().getId();
        final Long bouquetId = flowerInBouquet.getBouquet().getId();
        flowerInBouquet.setFlower(flowerRepository.findById(flowerId)
                .orElseThrow(() -> new ResourceNotFoundException(FLOWER_NOT_FOUND)));

        final FlowerInBouquetKey key = new FlowerInBouquetKey(bouquetId, flowerId);
        flowerInBouquet.setId(key);

        return flowerInBouquetRepository.save(flowerInBouquet);
    }
}