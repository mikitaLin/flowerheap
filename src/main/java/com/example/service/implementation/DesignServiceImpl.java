package com.example.service.implementation;

import com.example.exception.BadRequestException;
import com.example.exception.ResourceNotFoundException;
import com.example.model.entity.Design;
import com.example.repository.DesignRepository;
import com.example.service.DesignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DesignServiceImpl implements DesignService {

    private static final String DESIGN_EXIST = "This design was already created!";
    private static final String DESIGN_NOT_FOUND = "Design not found";

    private final DesignRepository designRepository;

    @Autowired
    public DesignServiceImpl(DesignRepository designRepository) {
        this.designRepository = designRepository;
    }

    @Override
    public Design create(Design design) {
        if (designRepository.existsByType(design.getType())) {
            throw new BadRequestException(DESIGN_EXIST);
        }
        design = designRepository.save(design);
        return design;
    }

    @Override
    public Page<Design> getAll(final Pageable pageable) {
        return designRepository.findAll(pageable);
    }

    @Override
    public Design getOne(final Long designId) {
        return designRepository.findById(designId)
                .orElseThrow(() -> new ResourceNotFoundException(DESIGN_NOT_FOUND));
    }

    @Override
    public Design update(final Long designId, final Design updatedDesign) {
        final String filename = updatedDesign.getFilename();
        return designRepository.findById(designId)
                .map(design -> {
                    design.setType(updatedDesign.getType());
                    design.setPrice(updatedDesign.getPrice());
                    design.setFilename(filename == null ? design.getFilename() : filename);
                    return design;
                })
                .orElseThrow(() -> new ResourceNotFoundException(DESIGN_NOT_FOUND));
    }

    @Override
    public void delete(final Long designId) {
        if (!designRepository.existsById(designId)) {
            throw new ResourceNotFoundException(DESIGN_NOT_FOUND);
        }
        designRepository.deleteById(designId);
    }

    @Override
    public Optional<Design> findOne(final Long designId) {
        return designRepository.findById(designId);
    }
}