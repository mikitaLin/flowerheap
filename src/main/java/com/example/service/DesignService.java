package com.example.service;

import com.example.model.entity.Design;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface DesignService extends CrudService<Design, Long> {

    Page<Design> getAll(Pageable pageable);

    Optional<Design> findOne(Long designId);
}