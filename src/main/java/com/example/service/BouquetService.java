package com.example.service;

import com.example.model.entity.Bouquet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface BouquetService extends CrudService<Bouquet, Long> {

    Bouquet create(Bouquet bouquet, Long orderId);

    Page<Bouquet> getAll(Pageable pageable, Long orderId, Long userId);

    Page<Bouquet> getAll(Pageable pageable, Authentication authentication, Long orderId);

    Bouquet getOne(Authentication authentication, Long bouquetId);
}