package com.example.service;

import com.example.model.OrderStatus;
import com.example.model.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface OrderService extends CrudService<Order, Long> {

    Order create(Order order, Authentication authentication);

    Page<Order> getAll(Pageable pageable, String phoneNumber, Long userId, OrderStatus status);

    Page<Order> getAll(Pageable pageable, Authentication authentication, OrderStatus status);

    Order getOne(Authentication authentication, Long orderId);

    Order changeStatus(Long orderId, Order order);
}