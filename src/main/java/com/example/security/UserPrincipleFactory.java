package com.example.security;

import com.example.model.Role;
import com.example.model.UserStatus;
import com.example.model.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public final class UserPrincipleFactory {

    public UserPrincipleFactory() {
    }

    static UserPrincipal createUserPrinciple(User user) {
        return new UserPrincipal(
                user.getId(),
                user.getCreated(),
                user.getUpdated(),
                user.getStatus(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                user.getActivationCode(),
                user.getFirstName(),
                user.getOrders(),
                user.getStatus().equals(UserStatus.ACTIVE),
                user.getUpdated(),
                mapToGrantedAuthorities(user.getRoles())
        );
    }

    private static Set<GrantedAuthority> mapToGrantedAuthorities(Set<Role> roles) {
        Set<GrantedAuthority> authorities = roles.stream()
                .map(role ->
                        new SimpleGrantedAuthority(role.name()))
                .collect(Collectors.toSet());

        return authorities;
    }
}