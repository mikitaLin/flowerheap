package com.example.security;

import com.example.model.UserStatus;
import com.example.model.entity.Order;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Getter
public class UserPrincipal implements UserDetails {

    private final Long id;
    private final Date created;
    private final Date updated;
    private final UserStatus status;
    private final String username;
    private final String password;
    private final String email;
    private final String activationCode;
    private final String firstName;
    private final boolean enabled;
    private final List<Order> orders;
    private final Date lastPasswordResetDate;
    private final Collection<? extends GrantedAuthority> authorities;

    UserPrincipal(Long id,
                  Date created,
                  Date updated,
                  UserStatus status,
                  String username,
                  String password,
                  String email,
                  String activationCode,
                  String firstName,
                  List<Order> orders,
                  boolean enabled,
                  Date lastPasswordResetDate,
                  Collection<? extends GrantedAuthority> authorities
    ) {
        this.id = id;
        this.created = created;
        this.updated = updated;
        this.status = status;
        this.username = username;
        this.password = password;
        this.email = email;
        this.activationCode = activationCode;
        this.firstName = firstName;
        this.orders = orders;
        this.enabled = enabled;
        this.lastPasswordResetDate = lastPasswordResetDate;
        this.authorities = authorities;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}