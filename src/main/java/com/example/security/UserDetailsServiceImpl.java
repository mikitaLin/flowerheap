package com.example.security;

import com.example.model.entity.User;
import com.example.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getOne(username);
        log.info("IN loadUserByUsername - user with username: {} successfully loaded", username);
        return UserPrincipleFactory.createUserPrinciple(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userService.getOne(id);
        log.info("IN loadUserById - user with id: {} successfully loaded", id);
        return UserPrincipleFactory.createUserPrinciple(user);
    }
}