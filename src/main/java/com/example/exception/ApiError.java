package com.example.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;

import static java.util.Collections.singletonList;

@Getter
class ApiError {

    private HttpStatus status;
    private String message;
    private List<String> errors;

    ApiError(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        errors = singletonList(error);
    }
}